//
//  PAGradientView.h
//  CollectionTest
//
//  Created by Павел Альбицкий on 19.08.15.
//  Copyright (c) 2015 Павел Альбицкий. All rights reserved.
//

// ----------------------------------------------------------------------------

#import <UIKit/UIKit.h>

// ----------------------------------------------------------------------------

IB_DESIGNABLE
@interface PAGradientView : UIView

// ----------------------------------------------------------------------------

@property(nonatomic, assign) IBInspectable CGFloat leftStartX;
@property(nonatomic, assign) IBInspectable CGFloat leftStartY;
@property(nonatomic, assign) IBInspectable CGFloat leftEndX;
@property(nonatomic, assign) IBInspectable CGFloat leftEndY;

@property(nonatomic, assign) IBInspectable CGFloat rightStartX;
@property(nonatomic, assign) IBInspectable CGFloat rightStartY;
@property(nonatomic, assign) IBInspectable CGFloat rightEndX;
@property(nonatomic, assign) IBInspectable CGFloat rightEndY;

@property(nonatomic, assign) IBInspectable CGFloat colorAlpha;

// ----------------------------------------------------------------------------

@end

// ----------------------------------------------------------------------------