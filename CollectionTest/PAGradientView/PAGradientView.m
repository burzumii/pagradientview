//
//  PAGradientView.m
//  CollectionTest
//
//  Created by Павел Альбицкий on 19.08.15.
//  Copyright (c) 2015 Павел Альбицкий. All rights reserved.
//

// ----------------------------------------------------------------------------

#import "PAGradientView.h"

// ----------------------------------------------------------------------------
#pragma mark - Default values
// ----------------------------------------------------------------------------

static CGFloat const PADefaultLeftStartX = 0;
static CGFloat const PADefaultLeftStartY = 0.5;
static CGFloat const PADefaultLeftEndX = 0.15;
static CGFloat const PADefaultLeftEndY = 0.5;

static CGFloat const PADefaultRightStartX = 1.0;
static CGFloat const PADefaultRightStartY = 0.5;
static CGFloat const PADefaultRightEndX = 0.85;
static CGFloat const PADefaultRightEndY = 0.5;

static CGFloat const PADefaultStartColorAlpha = 0.7;

// ----------------------------------------------------------------------------

@interface PAGradientView ()

// ----------------------------------------------------------------------------

@property(nonatomic, weak) CAGradientLayer *leftGradientLayer;
@property(nonatomic, weak) CAGradientLayer *rightGradientLayer;

// ----------------------------------------------------------------------------

@end

// ----------------------------------------------------------------------------

@implementation PAGradientView

// ----------------------------------------------------------------------------
#pragma mark - Default colors
// ----------------------------------------------------------------------------

+ (UIColor *)PADefaultStartColor {
    return [UIColor colorWithWhite:0.0 alpha:PADefaultStartColorAlpha];
}

// ----------------------------------------------------------------------------

+ (UIColor *)PADefaultEndColor {
    return [UIColor clearColor];
}

// ----------------------------------------------------------------------------

+ (NSArray *)defaultColors {
    return @[(__bridge id) [[PAGradientView PADefaultStartColor] CGColor],
            (__bridge id) [[PAGradientView PADefaultEndColor] CGColor]];
}

// ----------------------------------------------------------------------------
#pragma mark - initialization
// ----------------------------------------------------------------------------

static PAGradientView *CommonInit(PAGradientView *self) {
    if (self != nil) {
        self.leftGradientLayer = [self createLeftGradient];
        self.rightGradientLayer = [self createRightGradient];

        [self.layer addSublayer:self.leftGradientLayer];
        [self.layer addSublayer:self.rightGradientLayer];

        [self setNeedsDisplay];
    }
    return self;
}

// ----------------------------------------------------------------------------

- (instancetype)initWithCoder:(NSCoder *)decoder {
    return CommonInit([super initWithCoder:decoder]);
}

// ----------------------------------------------------------------------------

- (instancetype)initWithFrame:(CGRect)frame {
    return CommonInit([super initWithFrame:frame]);
}

// ----------------------------------------------------------------------------
#pragma mark - UIView
// ----------------------------------------------------------------------------

- (void)layoutSubviews {
    [super layoutSubviews];

    self.leftGradientLayer.frame = self.bounds;
    self.rightGradientLayer.frame = self.bounds;
}

// ----------------------------------------------------------------------------
#pragma mark - Create Layers
// ----------------------------------------------------------------------------

- (CAGradientLayer *)layerWithStartPoint:(CGPoint)startPoint endPoint:(CGPoint)endPoint {
    CAGradientLayer *gradientLayer = [CAGradientLayer layer];

    gradientLayer.frame = CGRectMake(0, 0, self.frame.size.width, self.frame.size.height);
    gradientLayer.startPoint = startPoint;
    gradientLayer.endPoint = endPoint;
    gradientLayer.colors = [PAGradientView defaultColors];

    return gradientLayer;
}

// ----------------------------------------------------------------------------

- (CAGradientLayer *)createLeftGradient {
    return [self layerWithStartPoint:CGPointMake(PADefaultLeftStartX, PADefaultLeftStartY)
                            endPoint:CGPointMake(PADefaultLeftEndX, PADefaultLeftEndY)];
}

// ----------------------------------------------------------------------------

- (CAGradientLayer *)createRightGradient {
    return [self layerWithStartPoint:CGPointMake(PADefaultRightStartX, PADefaultRightStartY)
                            endPoint:CGPointMake(PADefaultRightEndX, PADefaultRightEndY)];
}

// ----------------------------------------------------------------------------
#pragma mark - Setters Left Points
// ----------------------------------------------------------------------------

- (void)setLeftStartX:(CGFloat)leftStartX {
    _leftStartX = leftStartX;

    CGPoint point = self.leftGradientLayer.startPoint;
    point.x = _leftStartX;
    self.leftGradientLayer.startPoint = point;

    [self setNeedsDisplay];
}

// ----------------------------------------------------------------------------

- (void)setLeftStartY:(CGFloat)leftStartY {
    _leftStartY = leftStartY;

    CGPoint point = self.leftGradientLayer.startPoint;
    point.y = _leftStartY;
    self.leftGradientLayer.startPoint = point;

    [self setNeedsDisplay];
}

// ----------------------------------------------------------------------------

- (void)setLeftEndX:(CGFloat)leftEndX {
    _leftEndX = leftEndX;

    CGPoint point = self.leftGradientLayer.endPoint;
    point.x = _leftEndX;
    self.leftGradientLayer.endPoint = point;

    [self setNeedsDisplay];
}

// ----------------------------------------------------------------------------

- (void)setLeftEndY:(CGFloat)leftEndY {
    _leftEndY = leftEndY;

    CGPoint point = self.leftGradientLayer.endPoint;
    point.y = _leftEndY;
    self.leftGradientLayer.endPoint = point;

    [self setNeedsDisplay];
}

// ----------------------------------------------------------------------------
#pragma mark - Setters Right Points
// ----------------------------------------------------------------------------

- (void)setRightStartX:(CGFloat)rightStartX {
    _rightStartX = rightStartX;

    CGPoint point = self.rightGradientLayer.startPoint;
    point.x = _rightStartX;
    self.rightGradientLayer.startPoint = point;

    [self setNeedsDisplay];
}

// ----------------------------------------------------------------------------

- (void)setRightStartY:(CGFloat)rightStartY {
    _rightStartY = rightStartY;

    CGPoint point = self.rightGradientLayer.startPoint;
    point.y = _rightStartY;
    self.rightGradientLayer.startPoint = point;

    [self setNeedsDisplay];
}

// ----------------------------------------------------------------------------

- (void)setRightEndX:(CGFloat)rightEndX {
    _rightEndX = rightEndX;

    CGPoint point = self.rightGradientLayer.endPoint;
    point.x = _rightEndX;
    self.rightGradientLayer.endPoint = point;

    [self setNeedsDisplay];
}

// ----------------------------------------------------------------------------

- (void)setRightEndY:(CGFloat)rightEndY {
    _rightEndY = rightEndY;

    CGPoint point = self.rightGradientLayer.endPoint;
    point.y = _rightEndY;
    self.rightGradientLayer.endPoint = point;

    [self setNeedsDisplay];
}

// ----------------------------------------------------------------------------
#pragma mark - Setters Color
// ----------------------------------------------------------------------------

- (void)setColorAlpha:(CGFloat)colorAlpha {
    _colorAlpha = colorAlpha;

    self.leftGradientLayer.colors = self.rightGradientLayer.colors = @[
            (__bridge id) [[UIColor colorWithWhite:0.0 alpha:_colorAlpha] CGColor],
            (__bridge id) [[PAGradientView PADefaultEndColor] CGColor]];

    [self setNeedsDisplay];
}

// ----------------------------------------------------------------------------

@end

// ----------------------------------------------------------------------------