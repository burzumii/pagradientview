//
//  CustomCell.h
//  CollectionTest
//
//  Created by Павел Альбицкий on 18.08.15.
//  Copyright (c) 2015 Павел Альбицкий. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet CustomCell *colorView;

@end
