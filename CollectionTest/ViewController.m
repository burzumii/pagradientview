//
//  ViewController.m
//  CollectionTest
//
//  Created by Павел Альбицкий on 18.08.15.
//  Copyright (c) 2015 Павел Альбицкий. All rights reserved.
//

#import "ViewController.h"
#import "CustomCell.h"

#import <QuartzCore/QuartzCore.h>
#import <QuartzCore/CALayer.h>


@interface ViewController ()

@property (weak, nonatomic) IBOutlet UICollectionView *rowCollectionView;

@end

@implementation ViewController

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    ((UICollectionViewFlowLayout *)self.rowCollectionView.collectionViewLayout).minimumLineSpacing = 1.0f;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return 20;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    CustomCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"ReuseID" forIndexPath:indexPath];
    
    CGFloat hue = ( arc4random() % 256 / 256.0 );  //  0.0 to 1.0
    CGFloat saturation = ( arc4random() % 128 / 256.0 ) + 0.5;  //  0.5 to 1.0, away from white
    CGFloat brightness = ( arc4random() % 128 / 256.0 ) + 0.5;  //  0.5 to 1.0, away from black
    
    cell.colorView.backgroundColor = [UIColor colorWithHue:hue saturation:saturation brightness:brightness alpha:1];
    
    return cell;
}

@end
