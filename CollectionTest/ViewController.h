//
//  ViewController.h
//  CollectionTest
//
//  Created by Павел Альбицкий on 18.08.15.
//  Copyright (c) 2015 Павел Альбицкий. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController <UICollectionViewDataSource>


@end

