//
//  main.m
//  CollectionTest
//
//  Created by Павел Альбицкий on 18.08.15.
//  Copyright (c) 2015 Павел Альбицкий. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
